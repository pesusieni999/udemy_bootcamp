var express = require("express");
var app = express();
var rp = require("request-promise");
app.set("view engine", "ejs");

app.get("/", (req, res) => {
	res.render("search");
});

app.get("/results", (req, res) => {
	const searchTerm = req.query.search;
	rp(`https://www.omdbapi.com/?s=${searchTerm}&apikey=thewdb`)
	.then((body) => {			
		if (res.statusCode == 200) {
			const parsedData = JSON.parse(body);
			//res.send(`First movie is: ${parsedData.Search[0].Title}`);
			res.render("results", {
				parsedData: parsedData
			});
		}
	})
	.catch((error) => {

	})
});


const port = 8888//process.env.PORT;
const ip = process.env.IP;
app.listen(port, process.env.IP, () =>{
	console.log("Movie app has started");
});