const mongoose = require("mongoose");
const Post = require("./models/post");
const User = require("./models/user");

// Setup Database connection with Mongoose.
mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);
mongoose.connect("mongodb://localhost:27017/blog_demo");

/*User.create({
	email: "bob@gmail.com",
	name: "Bob Belcher"
}, (err, user) => {
	if (err) {
		console.log(err);
	} else {
		console.log(user);
	}
});*/

/*Post.create({
	title: "How to cook the best burger p2",
	content: "111111111111111111111111111 111111111"
}, (err, post) => {
	if (err) {
		console.log(err);
	} else {
		User.findOne({email: "bob@gmail.com"}, (err, foundUser) => {
			if (err) {
				console.log(err);
			} else {
				foundUser.posts.push(post);
				foundUser.save((err, data) => {
					if (err) {
						console.log(err);
					} else {
						console.log(data);
					}
				})
			}
		});
	}
});*/

User.findOne({email: "bob@gmail.com"}).populate("posts").exec((err, user) => {
	if (err) {
		console.log(err);
	} else {
		console.log(user);
	}
});