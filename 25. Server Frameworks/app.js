var express = require("express");
var app = express();

// ROUTES
app.get("/", function(req, res) {
	res.send("Hi there, welcome to my assignment");
});

app.get("/speak/:animal", function(req, res) {
	var sounds = {
		pig: "Oink",
		cow: "Moo",
		dog: "Woof"
	}

	var animal = req.params.animal.toLowerCase();
	var sound = sounds[animal];
	res.send("The " + animal + " says '" + sound + "'");
});

app.get("/repeat/:word/:times", function(req, res) {
	var response = "";
	var word = req.params.word;
	var times = Number(req.params.times);
	for (var i = 0; i < times; i++) {
		response += word + " ";
	}

	res.send(response);
});

app.get("*", function(req, res) {
	res.send("Sorry page not found...What are you doing iwth your life?");
});


// START SERVER
var port = process.env.PORT | 3000;
app.listen(port, process.env.IP, function() {
	console.log("Server started listening on port " + port);
});