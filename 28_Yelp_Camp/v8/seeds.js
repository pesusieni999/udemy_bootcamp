const mongoose = require("mongoose");
const Campground = require("./models/campground");
const Comment = require("./models/comment");

const campgroundData = [
	{
		name: "Cloud's rest",
		image: "https://images.unsplash.com/photo-1504280390367-361c6d9f38f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
		description: "Bacon ipsum dolor amet cupim shankle hamburger bacon capicola jowl meatloaf. Pork jerky kielbasa, burgdoggen sausage sirloin pancetta strip steak brisket turkey jowl flank tri-tip pastrami picanha. Ham venison pancetta shankle. Chuck spare ribs shoulder sausage venison pastrami capicola meatball. Short loin ball tip cow bacon, meatball fatback shankle landjaeger strip steak ham hock sirloin. Tail prosciutto tenderloin, corned beef turkey spare ribs chuck pork kevin porchetta biltong andouille cupim swine. Tenderloin buffalo drumstick chuck, salami filet mignon porchetta corned beef tri-tip sausage t-bone picanha."
	},
	{
		name: "Mountain Pass",
		image: "https://images.unsplash.com/photo-1478131143081-80f7f84ca84d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
		description: "Bacon ipsum dolor amet cupim shankle hamburger bacon capicola jowl meatloaf. Pork jerky kielbasa, burgdoggen sausage sirloin pancetta strip steak brisket turkey jowl flank tri-tip pastrami picanha. Ham venison pancetta shankle. Chuck spare ribs shoulder sausage venison pastrami capicola meatball. Short loin ball tip cow bacon, meatball fatback shankle landjaeger strip steak ham hock sirloin. Tail prosciutto tenderloin, corned beef turkey spare ribs chuck pork kevin porchetta biltong andouille cupim swine. Tenderloin buffalo drumstick chuck, salami filet mignon porchetta corned beef tri-tip sausage t-bone picanha."
	},
	{
		name: "Sleeping Pony",
		image: "https://images.unsplash.com/photo-1508873696983-2dfd5898f08b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
		description: "Bacon ipsum dolor amet cupim shankle hamburger bacon capicola jowl meatloaf. Pork jerky kielbasa, burgdoggen sausage sirloin pancetta strip steak brisket turkey jowl flank tri-tip pastrami picanha. Ham venison pancetta shankle. Chuck spare ribs shoulder sausage venison pastrami capicola meatball. Short loin ball tip cow bacon, meatball fatback shankle landjaeger strip steak ham hock sirloin. Tail prosciutto tenderloin, corned beef turkey spare ribs chuck pork kevin porchetta biltong andouille cupim swine. Tenderloin buffalo drumstick chuck, salami filet mignon porchetta corned beef tri-tip sausage t-bone picanha."
	}
];

function seedDB() {
	// Remove all campgrounds
	Campground.deleteMany({}, (err) => {
		if (err) {
			console.log(err);
		} else {
			console.log("Removed campgrounds");

			Comment.deleteMany({}, (err) => {
				if (err) {
					console.log(err);
				} else {
					console.log("Removed comments");

					campgroundData.forEach((seed) => {
						Campground.create(seed, (err, campground) => {
							if (err) {
								console.log(err);
							} else {
								console.log("Added a campground");

								// Create comments.
								Comment.create(
										{
											text: "This place is great, but I wish there was internet",
											author: "Homer"
										}, (err, comment) => {
											if (err) {
												console.log(err);
											} else {
												campground.comments.push(comment);
												campground.save();
												console.log("Added comment");
											}
										}
									);
							}
						});
					});
				}
			})
		}
	})
}

module.exports = seedDB;