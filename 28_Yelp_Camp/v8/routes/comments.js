const express = require("express");
const router = express.Router({ mergeParams: true});

const Campground = require("../models/campground");
const Comment = require("../models/comment");

// Comments new
router.get("/new", isLoggedIn, (req, res) => {
	Campground.findById(req.params.id, (err, campground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("comments/new", {campground: campground});
		}
	});
});

// Comments create
router.post("/", isLoggedIn, (req, res) => {
	Campground.findById(req.params.id, (err, campground) => {
		if (err) {
			return res.redirect("/campgrounds");
		}

		Comment.create(req.body.comment, (err, comment) => {
			if (err) {
				return res.redirect("/campgrounds");
			} 

			comment.author.id = req.user._id;
			comment.author.username = req.user.username;
			comment.save();
			
			campground.comments.push(comment);
			campground.save();
			res.redirect("/campgrounds/" + req.params.id);	
		});
	});
});

// MIDDLEWARE FUNCTION DEFINITIONS
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect("/login");
}

module.exports = router;