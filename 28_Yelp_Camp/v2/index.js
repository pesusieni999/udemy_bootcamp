const express = require("express");
const app = express();
const rp = require("request-promise");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

// Handle different deprecation calls.
mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);
mongoose.connect("mongodb://localhost:27017/yelpcamp");

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
	console.log("Connected to DB.");
})

// Schema setup:
const campgroundSchema = new mongoose.Schema({
	name: String,
	image: String,
	description: String
});

const Campground = mongoose.model("Campground", campgroundSchema);

// Create combines new and save function calls.
/*Campground.create({
	name: "Their home",
	image: "https://www.photosforclass.com/download/pixabay-1031141?webUrl=https%3A%2F%2Fpixabay.com%2Fget%2F57e0d6424b56ad14f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_960.jpg&user=Free-Photos",
	description: "Really nice place"
}, (err, campground) => {
	if (err) {
		console.log("Error occured: ", err);
	} else {
		console.log("New campground added: ", campground);
	}
});*/

app.get("/", (req, res) => {
	res.render("backup");
});

// INDEX REST
app.get("/campgrounds", (req, res) => {
	Campground.find({}, (err, allCampgrounds) => {
		if (err) {
			console.log(err);
		} else {
			res.render("index", {campgrounds: allCampgrounds});
		}
	});
});

// CREATE REST
app.post("/campgrounds", (req, res) => {
	const name = req.body.name;
	const image = req.body.image;
	const description = req.body.description

	Campground.create({name: name, image: image, description: description}, (err, campground) => {
		if (err) {
			console.log(err);
		} else {
			res.redirect("/campgrounds");
		}
	});	
});

// NEW REST
app.get("/campgrounds/new", (req, res) => {
	res.render("new");
});

// SHOW REST - Shows more info about one campground
app.get("/campgrounds/:id", (req, res) => {
	Campground.findById(req.params.id, (err, foundCampground) => {
		if (err) {
			console.log(err);
		} else {
			res.render("show", {campground: foundCampground});
		}
	});
});

app.listen(8888, process.env.IP, () => {
	console.log("Yelp camp server has started");
});

/*
// Old datastructure
var campGrounds = [
	{
		name: "Salmon Creek",
		image: "https://www.photosforclass.com/download/pixabay-1149402?webUrl=https%3A%2F%2Fpixabay.com%2Fget%2F57e1d14a4e52ae14f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_960.jpg&user=Free-Photos"
	},
	{
		name: "Granite Hill",
		image: "https://pixabay.com/get/57e8d1454b56ae14f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_340.jpg"
	},
	{
		name: "Mountain Goat's Rest",
		image: "https://pixabay.com/get/57e8d1464d53a514f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_340.jpg"
	}
];

// Define campground schema and builder.
mongoose.connect("mongodb://localhost/yelpcamp");
const campSchema = new mongoose.Schema({
	name: String,
	image: String,
	isActive: Boolean
});
const Camp = mongoose.model("Campground", campschema);

// Create a new campground and store it to DB.
const yourCamp = new Camp({
	name: "Your home",
	image: "https://pixabay.com/get/57e1dd4a4350a514f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_340.jpg",
	isActive: true
})
yourCamp.save((err, camp) => {
	if (err) {
		console.log("Something went wrong: ", err);
	} else {
		console.log("New camp created: ", camp);
	}
});

// Create combines new and save function calls.
Cat.create({
	name: "Their home",
	image: "https://www.photosforclass.com/download/pixabay-1031141?webUrl=https%3A%2F%2Fpixabay.com%2Fget%2F57e0d6424b56ad14f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_960.jpg&user=Free-Photos",
	isActive: false;
}, (err, campground) => {
	if (err) {
		console.log("Error occured: ", err);
	} else {
		console.log("New campground added: ", campground);
	}
});

// Retrieve all campgrounds.
Campground.find({}, (err, campgrounds) => {
	if (err) {
		console.log("Error occured: ", err);
	} else {
		console.log("Campgrounds found: ", campgrounds);
	}
});
*/