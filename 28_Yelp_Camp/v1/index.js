const express = require("express");
const app = express();
const rp = require("request-promise");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");




/*const mongoUri = "mongodb+srv://dbUser:TestiPallo!234@cluster0-jyhln.mongodb.net/test?retryWrites=true&w=majority";
const MongoClient = require('mongodb').MongoClient;

// replace the uri string with your connection string.
const uri = "mongodb+srv://shahid:<PASSWORD>@cluster0-1q7ty.mongodb.net/test"
MongoClient.connect(uri, function(err, client) {
   if(err) {
        console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
   }
   console.log('Connected...');
   const collection = client.db("test").collection("devices");
   // perform actions on the collection object
   client.close();
});*/

var campGrounds = [
	{
		name: "Salmon Creek",
		image: "https://www.photosforclass.com/download/pixabay-1149402?webUrl=https%3A%2F%2Fpixabay.com%2Fget%2F57e1d14a4e52ae14f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_960.jpg&user=Free-Photos"
	},
	{
		name: "Granite Hill",
		image: "https://pixabay.com/get/57e8d1454b56ae14f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_340.jpg"
	},
	{
		name: "Mountain Goat's Rest",
		image: "https://pixabay.com/get/57e8d1464d53a514f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_340.jpg"
	}
];

app.get("/", (req, res) => {
	res.render("index");
});

app.get("/campgrounds", (req, res) => {
	res.render("campgrounds", {
		campgrounds: campGrounds
	});
});

app.post("/campgrounds", (req, res) => {
	const name = req.body.name;
	const image = req.body.image;
	campGrounds.push({
		name: name,
		image: image
	});
	res.redirect("/campgrounds");
});

app.get("/campgrounds/new", (req, res) => {
	res.render("new");
});

app.listen(8888, process.env.IP, () => {
	console.log("Yelp camp server has started");
});

/*
// Define campground schema and builder.
mongoose.connect("mongodb://localhost/yelpcamp");
const campSchema = new mongoose.Schema({
	name: String,
	image: String,
	isActive: Boolean
});
const Camp = mongoose.model("Campground", campschema);

// Create a new campground and store it to DB.
const yourCamp = new Camp({
	name: "Your home",
	image: "https://pixabay.com/get/57e1dd4a4350a514f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_340.jpg",
	isActive: true
})
yourCamp.save((err, camp) => {
	if (err) {
		console.log("Something went wrong: ", err);
	} else {
		console.log("New camp created: ", camp);
	}
});

// Create combines new and save function calls.
Cat.create({
	name: "Their home",
	image: "https://www.photosforclass.com/download/pixabay-1031141?webUrl=https%3A%2F%2Fpixabay.com%2Fget%2F57e0d6424b56ad14f6da8c7dda793f7f1636dfe2564c704c73277dd59644c65c_960.jpg&user=Free-Photos",
	isActive: false;
}, (err, campground) => {
	if (err) {
		console.log("Error occured: ", err);
	} else {
		console.log("New campground added: ", campground);
	}
});

// Retrieve all campgrounds.
Campground.find({}, (err, campgrounds) => {
	if (err) {
		console.log("Error occured: ", err);
	} else {
		console.log("Campgrounds found: ", campgrounds);
	}
});
*/