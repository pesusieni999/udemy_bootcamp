const express = require("express");
const router = express.Router({ mergeParams: true});

const Campground = require("../models/campground");
const Comment = require("../models/comment");
const middleware = require("../middleware")

// Comments new
router.get("/new", middleware.isLoggedIn, (req, res) => {
	Campground.findById(req.params.id, (err, campground) => {
		if (err) {
			req.flash("error", "Failed to find campground");
			res.redirect("/campgrounds");
		} else {
			res.render("comments/new", {campground: campground});
		}
	});
});

// Comments create
router.post("/", middleware.isLoggedIn, (req, res) => {
	Campground.findById(req.params.id, (err, campground) => {
		if (err) {
			req.flash("error", "Failed to find campground");
			return res.redirect("/campgrounds");
		}

		Comment.create(req.body.comment, (err, comment) => {
			if (err) {
				req.flash("error", "Failed to create comment");
				return res.redirect("/campgrounds");
			} 

			comment.author.id = req.user._id;
			comment.author.username = req.user.username;
			comment.save();
			
			campground.comments.push(comment);
			campground.save();
			res.redirect("/campgrounds/" + req.params.id);	
		});
	});
});

// Comments update
router.put("/:comment_id", middleware.checkCommentOwner, (req, res) => {
	Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, (err, comment) => {
		if (err) {
			req.flash("error", "Failed to update comment");
			return res.redirect("back");
		}

		res.redirect("/campgrounds/" + req.params.id);
	});
});

// Commente delete
router.delete("/:comment_id", middleware.checkCommentOwner, (req, res) => {
	Comment.findByIdAndRemove(req.params.comment_id, (err) => {
		if (err) {
			req.flash("error", "Failed to delete comment");
			return res.redirect("back");
		}

		req.flash("success", "Comment deleted");
		res.redirect("/campgrounds/" + req.params.id);
	});
});

// Comments edit
router.get("/:comment_id/edit", middleware.checkCommentOwner, (req, res) => {
	Comment.findById(req.params.comment_id, (err, comment) => {
		if (err) {
			req.flash("error", "Failed to find comment");
			return res.redirect("back");
		}

		res.render("comments/edit", {
			campground_id: req.params.id,
			comment: comment
		});
	});
});

module.exports = router;