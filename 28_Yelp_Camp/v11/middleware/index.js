const Campground = require("../models/campground");
const Comment = require("../models/comment");


let middlewareObj = {};

// MIDDLEWARE FUNCTION DEFINITIONS
middlewareObj.isLoggedIn = function(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	req.flash("error", "You need to be logged in to do that");
	res.redirect("/login");
}

middlewareObj.checkCampgroundOwner = function(req, res, next) {
	if (req.isAuthenticated()) {
		Campground.findById(req.params.id, (err, campground) => {
			if (err) {
				req.flash("error", "Failed to find campground");
				res.redirect("back");
			} else {
				if (campground.author.id.equals(req.user._id)) {
					next();
				} else {
					req.flash("error", "You don't have permission to do that");
					res.redirect("back");
				}
			}
		});
	} else {
		req.flash("error", "You need to be logged in to do that");
		res.redirect("back");
	}
}

middlewareObj.checkCommentOwner = function(req, res, next) {
	if (req.isAuthenticated()) {
		Comment.findById(req.params.comment_id, (err, comment) => {
			if (err) {
				req.flash("error", "Failed to find comment");
				res.redirect("back");
			} else {
				if (comment.author.id.equals(req.user._id)) {
					next()
				} else {
					req.flash("error", "You don't have permission to do that");
					res.redirect("back");
				}
			}
		});
	} else {
		req.flash("error", "You need to be logged in to do that");
		res.redirect("back");
	}
}

module.exports = middlewareObj;