const express = require("express");
const app = express();
const rp = require("request-promise");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("passport");
const LocalStrategy = require("passport-local");

// APP MODELS
const Campground = require("./models/campground")
const seedDB = require("./seeds");
const Comment = require("./models/comment");
const User = require("./models/user");

// APP CONFIGS
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));

// PASSPORT CONFIGURATION
app.use(require("express-session") ({
	secret: "This is a secret",
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use((req, res, next) => {
	res.locals.currentUser = req.user;
	next();
});

// Handle different deprecation calls.
mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);
mongoose.connect("mongodb://localhost:27017/yelpcamp");

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
	console.log("Connected to DB.");
})

// Remove all data and seed database.
//seedDB();

// RESTFUL ROUTES
app.get("/", (req, res) => {
	res.render("landing");
});

// INDEX REST
app.get("/campgrounds", (req, res) => {
	Campground.find({}, (err, allCampgrounds) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("campgrounds/index", {campgrounds: allCampgrounds});
		}
	});
});

// CREATE REST
app.post("/campgrounds", (req, res) => {
	const name = req.body.name;
	const image = req.body.image;
	const description = req.body.description

	Campground.create({name: name, image: image, description: description}, (err, campground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.redirect("/campgrounds");
		}
	});	
});

// NEW REST
app.get("/campgrounds/new", (req, res) => {
	res.render("new");
});

// SHOW REST - Shows more info about one campground
app.get("/campgrounds/:id", (req, res) => {
	Campground.findById(req.params.id).populate("comments").exec((err, foundCampground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("campgrounds/show", {campground: foundCampground});
		}
	});
});


// ######### COMMENTS ROUTES ###########
// NEW COMMENT
app.get("/campgrounds/:id/comments/new", isLoggedIn, (req, res) => {
	Campground.findById(req.params.id, (err, campground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("comments/new", {campground: campground});
		}
	});
});

// CREATE COMMENT
app.post("/campgrounds/:id/comments", isLoggedIn, (req, res) => {
	Campground.findById(req.params.id, (err, campground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			Comment.create(req.body.comment, (err, comment) => {
				if (err) {
					res.redirect("/campgrounds");
				} else {
					campground.comments.push(comment);
					campground.save();
					res.redirect("/campgrounds/" + req.params.id);	
				}
			});
		}
	});
});


// AUTH ROUTES

// SHOW REGISTER
app.get("/register", (req, res) => {
	res.render("register");
});

// CREATE USER
app.post("/register", (req, res) => {
	var newUser = new User({username: req.body.username});
	User.register(newUser, req.body.password, (err, user) => {
		if (err) {
			console.log("err");
			return res.render("register");
		}

		passport.authenticate("local")(req, res, () => {
			res.redirect("/campgrounds");
		})
	})
});

// SHOW LOGIN
app.get("/login", (req, res) => {
	res.render("login");
});

// POST LOGIN
app.post("/login", passport.authenticate("local", 
	{
		successRedirect: "/campgrounds",
		failureRedirect: "/login"
	}), (req, res) => {
	res.send("login");
});

// LOGOUT
app.get("/logout", (req, res) => {
	req.logout();
	return res.redirect("/campgrounds");
});

// MIDDLEWARE FUNCTION DEFINITIONS
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect("/login");
}

// START SERVER
app.listen(8888, process.env.IP, () => {
	console.log("Yelp camp server has started");
});
