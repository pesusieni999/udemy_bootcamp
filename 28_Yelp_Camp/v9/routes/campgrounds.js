const express = require("express");
const router = express.Router();

const Campground = require("../models/campground");
const Comment = require("../models/comment");
const middleware = require("../middleware/index");

// INDEX REST
router.get("/", (req, res) => {
	Campground.find({}, (err, allCampgrounds) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("campgrounds/index", {campgrounds: allCampgrounds});
		}
	});
});

// CREATE REST
router.post("/", middleware.isLoggedIn, (req, res) => {
	const name = req.body.name;
	const image = req.body.image;
	const description = req.body.description
	const author = {
		id: req.user._id,
		username: req.user.username
	};

	Campground.create({name: name, image: image, description: description, author: author}, (err, campground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.redirect("/campgrounds");
		}
	});	
});

// NEW REST
router.get("/new", middleware.isLoggedIn, (req, res) => {
	res.render("campgrounds/new");
});

// SHOW REST - Shows more info about one campground
router.get("/:id", (req, res) => {
	Campground.findById(req.params.id).populate("comments").exec((err, foundCampground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("campgrounds/show", {campground: foundCampground});
		}
	});
});

// EDIT CAMPGROUND
router.get("/:id/edit", middleware.checkCampgroundOwner, (req, res) => {
		Campground.findById(req.params.id, (err, campground) => {
			res.render("campgrounds/edit", {campground: campground});			
		});
});

// UPDATE CAMPGROUNG
router.put("/:id", middleware.checkCampgroundOwner, (req, res) => {
	Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, campground) => {
		if (err) {
			console.log(err);
			return res.redirect("/campgrounds");
		}
		console.log(campground);
		res.redirect("/campgrounds/" + req.params.id);
	})
});

// DESTROY CAMPGROUNG
router.delete("/:id", middleware.checkCampgroundOwner, (req, res) => {
	Campground.findById(req.params.id, (err, campground) => {
		if (err) {
			console.log(err);
			return res.redirect("/campgrounds");
		}
		campground.remove();
		res.redirect("/campgrounds");
	});
});

module.exports = router;