const express = require("express");
const app = express();
const rp = require("request-promise");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

// APP MODELS
const Campground = require("./models/campground")
const seedDB = require("./seeds");
/*const Comment = require("./models/comment");
const User = require("./models/user");*/

// APP CONFIGS
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

// Handle different deprecation calls.
mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);
mongoose.connect("mongodb://localhost:27017/yelpcamp");

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
	console.log("Connected to DB.");
})

// Seed database.
seedDB();

// RESTFUL ROUTES
app.get("/", (req, res) => {
	res.render("backup");
});

// INDEX REST
app.get("/campgrounds", (req, res) => {
	Campground.find({}, (err, allCampgrounds) => {
		if (err) {
			console.log(err);
		} else {
			res.render("index", {campgrounds: allCampgrounds});
		}
	});
});

// CREATE REST
app.post("/campgrounds", (req, res) => {
	const name = req.body.name;
	const image = req.body.image;
	const description = req.body.description

	Campground.create({name: name, image: image, description: description}, (err, campground) => {
		if (err) {
			console.log(err);
		} else {
			res.redirect("/campgrounds");
		}
	});	
});

// NEW REST
app.get("/campgrounds/new", (req, res) => {
	res.render("new");
});

// SHOW REST - Shows more info about one campground
app.get("/campgrounds/:id", (req, res) => {
	Campground.findById(req.params.id).populate("comments").exec((err, foundCampground) => {
		if (err) {
			console.log(err);
		} else {
			res.render("show", {campground: foundCampground});
		}
	});
});

// START SERVER
app.listen(8888, process.env.IP, () => {
	console.log("Yelp camp server has started");
});
