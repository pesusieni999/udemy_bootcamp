const express = require("express");
const router = express.Router();
const passport = require("passport");

const User = require("../models/user");

// show root route
router.get("/", (req, res) => {
	res.render("landing");
});

// SHOW REGISTER
router.get("/register", (req, res) => {
	res.render("register");
});

// CREATE USER
router.post("/register", (req, res) => {
	var newUser = new User({username: req.body.username});
	User.register(newUser, req.body.password, (err, user) => {
		if (err) {
			req.flash("error", err.message);
			return res.redirect("/register");
		}

		passport.authenticate("local")(req, res, () => {
			req.flash("success", "Welcome to YelpCamp " + user.username);
			res.redirect("/campgrounds");
		})
	})
});

// SHOW LOGIN
router.get("/login", (req, res) => {
	res.render("login");
});

// POST LOGIN
router.post("/login", passport.authenticate("local", 
	{
		successRedirect: "/campgrounds",
		failureRedirect: "/login"
	}), (req, res) => {
	res.send("login");
});

// LOGOUT
router.get("/logout", (req, res) => {
	req.logout();
	req.flash("success", "You logged out");
	return res.redirect("/campgrounds");
});

module.exports = router;