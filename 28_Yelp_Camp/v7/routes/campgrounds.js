const express = require("express");
const router = express.Router();

const Campground = require("../models/campground");
const Comment = require("../models/comment");

// INDEX REST
router.get("/", (req, res) => {
	Campground.find({}, (err, allCampgrounds) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("campgrounds/index", {campgrounds: allCampgrounds});
		}
	});
});

// CREATE REST
router.post("/", (req, res) => {
	const name = req.body.name;
	const image = req.body.image;
	const description = req.body.description

	Campground.create({name: name, image: image, description: description}, (err, campground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.redirect("/campgrounds");
		}
	});	
});

// NEW REST
router.get("/new", (req, res) => {
	res.render("new");
});

// SHOW REST - Shows more info about one campground
router.get("/:id", (req, res) => {
	Campground.findById(req.params.id).populate("comments").exec((err, foundCampground) => {
		if (err) {
			res.redirect("/campgrounds");
		} else {
			res.render("campgrounds/show", {campground: foundCampground});
		}
	});
});

module.exports = router;