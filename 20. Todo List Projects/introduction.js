// Check off specific Todos by clicking
$("ul").on("click", "li", function() {
	$(this).toggleClass("completed");
});

$("ul").on("click", "li span", function(e) {
	// Remove the todo.
	$(this).parent().fadeOut(500, function() {
		$(this).remove();
	});

	// Finish event handling.
	e.stopPropagation();
});

$("input[type='text']").on("keypress", function(e) {
	if (e.which === 13) {
		var todoText = $(this).val();
		console.log(todoText);
		$("ul").append("<li><span><i class='fas fa-trash'></i></span> " + todoText + "</li>");

		$(this).val("");
	}
});

$(".fa-plus").on("click", function() {
	$("input[type='text']").fadeToggle();
});