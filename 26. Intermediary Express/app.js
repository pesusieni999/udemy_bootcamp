var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended: true}));

var friends = ["Mark", "Tony", "Lisa"];

// ROUTES
app.get("/", function(req, res) {
	res.render("index");
});

app.get("/friends", function(req, res) {
	res.render("friends", {
		friends: friends
	});
});

app.post("/addfriend", function(req, res) {
	var newFriend = req.body.name;
	console.log(newFriend);
	friends.push(newFriend);
	res.redirect("/friends");
});

app.get("/r/:name", function(req, res) {
	var thing = req.params.name;
	res.render("thing", {
		thingVar : thing
	});
});

app.get("/posts", function(req, res) {
	var posts = [
		{title: "Post 1", author: "Susy"},
		{title: "Post 2", author: "John"},
		{title: "Post 3", author: "Mark"}
	]

	res.render("posts", {
		posts: posts
	});
});

app.get("*", function(req, res) {
	res.send("Sorry page not found...What are you doing iwth your life?");
});

// START SERVER
var port = process.env.PORT | 3000;
app.listen(port, process.env.IP, function() {
	console.log("Server started listening on port " + port);
});